package weather

type (
	CityID string

	WeatherProvider interface {
		Fact(CityID) (*Weather, error)
	}

	Weather struct {
		Temperature float32 // C
		Humidity    float32
		Pressure    float32 // hPa
	}
)
