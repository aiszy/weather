package weather

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/fasales/fasthttp-modified"
	"github.com/PuerkitoBio/goquery"
)

const DarkSkyUrl = "https://darksky.net/forecast/%s/si12/en"

type DarkSkyScrapingProvider struct {
}

func NewDarkSkyScrapingProvider() *DarkSkyScrapingProvider {
	return &DarkSkyScrapingProvider{}
}

func (d DarkSkyScrapingProvider) Fact(id CityID) (*Weather, error) {
	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(resp)

	req.SetRequestURI(fmt.Sprintf(DarkSkyUrl, id))

	if err := setHeadersFasthttp(req, "chrome_87.0.4280.141_win10_navigate", ""); err != nil {
		return nil, err
	}

	err := fasthttp.Do(req, resp)
	if err != nil {
		return nil, err
	}

	respBody := make([]byte, 0)
	switch string(resp.Header.Peek("Content-Encoding")) {
	case "gzip":
		respBody, err = resp.BodyGunzip()
		if err != nil {
			return nil, err
		}
		break
	default:
		respBody = resp.Body()
	}

	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(respBody))
	if err != nil {
		return nil, err
	}

	temp := doc.Find(".currently span.summary").Text()
	temp = strings.Split(temp, "˚")[0]

	tempInt, err := strconv.Atoi(temp)
	if err != nil {
		return nil, err
	}

	humidity := doc.Find(".humidity__value").Text()
	humidityInt, err := strconv.Atoi(humidity)
	if err != nil {
		return nil, err
	}

	pressure := doc.Find(".pressure__value").Text()
	pressureInt, err := strconv.Atoi(pressure)
	if err != nil {
		return nil, err
	}

	return &Weather{
		Temperature: float32(tempInt),
		Humidity:    float32(humidityInt),
		Pressure:    float32(pressureInt),
	}, nil
}
