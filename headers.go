package weather

import (
	"errors"
	"strings"

	"bitbucket.org/fasales/fasthttp-modified"
)

var ErrorHeadersNotFound = errors.New("headers not found, the key does not exist")
var ErrorUrlIsEmpty = errors.New("first define url before calling SetCookiesFasthttp")

type header [][2]string

var headers = map[string]header{
	// Browser: Chrome 87.0.4280.141
	// OS: Windows 10
	"chrome_87.0.4280.141_win10_navigate": {
		{"Host", ""},
		{"Connection", "keep-alive"},
		{"Upgrade-Insecure-Requests", "1"},
		{"User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36"},
		{"Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"},
		{"Sec-Fetch-Site", "none"},
		{"Sec-Fetch-Mode", "navigate"},
		{"Sec-Fetch-User", "?1"},
		{"Sec-Fetch-Dest", "document"},
		{"Accept-Encoding", "gzip, deflate, br"},
		{"Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6,es;q=0.5,de;q=0.4"},
		{"Cookie", ""},
	},
	// Browser: Chrome 87.0.4280.141
	// OS: Windows 10
	"chrome_87.0.4280.141_win10_xhr": {
		{"Host", ""},
		{"Connection", "keep-alive"},
		{"Upgrade-Insecure-Requests", "1"},
		{"User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36"},
		{"Accept", "*/*"},
		{"Sec-Fetch-Site", "none"},
		{"Sec-Fetch-Mode", "cors"},
		{"Sec-Fetch-Dest", "empty"},
		{"Accept-Encoding", "gzip, deflate, br"},
		{"Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6,es;q=0.5,de;q=0.4"},
		{"Cookie", ""},
	},
}


func setHeadersFasthttp(req *fasthttp.Request, key string, cookie string) error {
	headersList, exist := headers[key]
	if !exist {
		return ErrorHeadersNotFound
	}

	if len(req.RequestURI()) == 0 {
		return ErrorUrlIsEmpty
	}

	for _, row := range headersList {
		if strings.ToLower(row[0]) == "host" {
			req.Header.Set(row[0], string(req.URI().Host()))
			continue
		}

		if strings.ToLower(row[0]) == "cookie" {
			if cookie == "" {
				continue
			}
			req.Header.Set(row[0], cookie)
			continue
		}

		req.Header.Set(row[0], row[1])
	}

	return nil
}
