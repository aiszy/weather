package main

import (
	"log"

	"bitbucket.org/aiszy/weather"
)

func main() {
	gm := weather.NewGismeteoExtProvider()
	log.Println(gm.Fact("7124"))

	ds := weather.NewDarkSkyScrapingProvider()
	log.Println(ds.Fact("35.2217,-80.8397"))
}
