package weather

import (
	"encoding/xml"
	"fmt"

	"bitbucket.org/fasales/fasthttp-modified"
)

const GiemeteoUrl = "https://services.gismeteo.ru/inform-service/inf_chrome/forecast/?city=%s&lang=en"

type GismeteoExtProvider struct {
}

type GismetioResp struct {
	XMLName  xml.Name `xml:"weather"`
	Location struct {
		Fact struct {
			Valid  string `xml:"valid,attr"`
			Values struct {
				Temp      int     `xml:"t,attr"`
				TempFloat float32 `xml:"tflt,attr"`
				Pressure  int     `xml:"p,attr"`
				Humidity  int     `xml:"hum,attr"`
			} `xml:"values"`
		} `xml:"fact"`
	} `xml:"location"`
}

func NewGismeteoExtProvider() *GismeteoExtProvider {
	return &GismeteoExtProvider{}
}

func (g GismeteoExtProvider) Fact(id CityID) (*Weather, error) {
	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(resp)

	req.SetRequestURI(fmt.Sprintf(GiemeteoUrl, id))

	if err := setHeadersFasthttp(req, "chrome_87.0.4280.141_win10_xhr", ""); err != nil {
		return nil, err
	}

	err := fasthttp.Do(req, resp)
	if err != nil {
		return nil, err
	}

	respBody := make([]byte, 0)
	switch string(resp.Header.Peek("Content-Encoding")) {
	case "gzip":
		respBody, err = resp.BodyGunzip()
		if err != nil {
			return nil, err
		}
		break
	default:
		respBody = resp.Body()
	}

	r := GismetioResp{}
	if err := xml.Unmarshal(respBody, &r); err != nil {
		return nil, err
	}

	return &Weather{
		Temperature: r.Location.Fact.Values.TempFloat,
		Humidity:    float32(r.Location.Fact.Values.Humidity),
		Pressure:    float32(r.Location.Fact.Values.Pressure) * 1.3332,
	}, nil
}
