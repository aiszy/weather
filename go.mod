module bitbucket.org/aiszy/weather

go 1.14

require (
	bitbucket.org/fasales/fasthttp-modified v1.0.1
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/valyala/fasthttp v1.19.0 // indirect
)
